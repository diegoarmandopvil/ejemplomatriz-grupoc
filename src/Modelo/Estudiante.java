/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Esta clase maneja la información de un estudiante con sus notas de quices
 * @author Marco Adarme
 */
public class Estudiante {
    
    private long codigo;
    private String nombre;
    private float quices[];

    
    /**
     *  Constructor vacío de la clase Estudiante
     */
    public Estudiante() {
    }

    /**
     *  Constructor de la clase estudiante
     * @param codigo un ID que representa la identificación dentro de la UFPS
     * @param nombre  El nombre del estudiante
     */
    public Estudiante(long codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }
    
    
    
    /**
     * Constructor de la clase estudiante
     * @param codigo un ID que representa la identificación dentro de la UFPS
     * nombre  El nombre del estudiante
     * @param listaNotas Un string separado por "," que contiene el listado de calificaciones (quices)
     */
    public Estudiante(long codigo, String nombre, String listaNotas) {
        this.codigo = codigo;
        this.nombre = nombre;
        cargarNotas(listaNotas);
        
        
        
    }
    
    private void cargarNotas(String listaNotas)
    {
        String notas[]=listaNotas.split(",");
        //Debo crear el vector de notas , 
        int tamQuices=notas.length;
        //Creamos el vector de notas
        
        //T ident[]=new T[tamaño]
        this.quices=new float[tamQuices];
        
        //Insertamos las notas en el vector de quices
        
        for(int i=0;i<this.quices.length;i++)
            this.quices[i]=Float.parseFloat(notas[i]);
        
    }
    
    
    /**
     *  Retorna el código de un estudiante (ID)
     * @return un número long con el ID UFPS
     */
    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float[] getQuices() {
        return quices;
    }

    public void setQuices(float[] quices) {
        this.quices = quices;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (int) (this.codigo ^ (this.codigo >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        
        String msg="";
        msg="Nombre:"+this.getNombre()+",Código:"+this.getCodigo()+", Mis notas son:";
        //falta los quices...
        
        //Foreach
        for(float notas:this.quices)
               msg+=notas+",";
        
        
        return msg;
        
        
    }
    
    
    /**
     * Obtiene el promedio del estudiante
     * @return un valor de tipo float con el promedio de notas del estudiante
     */
    public float getPromedio(){
        float promedio=0f;
        for(int i=0; i<this.quices.length; i++){
            promedio += this.quices[i];
        }
        return promedio/this.quices.length;
    }
    
    
    
    
    
    
}
