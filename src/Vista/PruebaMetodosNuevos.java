/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.SistemaCalificaciones;
import java.io.IOException;


/**
 *
 * @author madar
 */
public class PruebaMetodosNuevos {
    
        
    public static void main(String[] args) throws IOException {
        SistemaCalificaciones sistema=new SistemaCalificaciones("src/Datos/estudiantes.xls");
                
        //Metodo para saber estudiantes con promedio inferior a 3.0
        Estudiante [] reprobados = sistema.getReprobaronQuices();
        System.out.println("\n Estudiantes con promedio inferior a 3.0:");
        for (int i = 0; i < reprobados.length; i++) {
            System.out.println(reprobados[i].toString());
        }

        //Metodo para saber estudiantes con promedio igual o superior a 4.0
        Estudiante [] aprobados = sistema.getMayorNotaQuices();
        System.out.println("\n Estudiantes con promedio superior o igual a 4.0:");
        for (int i = 0; i < aprobados.length; i++) {
            System.out.println(aprobados[i].toString());
        }

        //Metodo para saber cual es la nota que mas se repite entre todos los quices
        Float notaMasRepetidaa = sistema.getNota_Que_MasRepite();
        System.out.println("\n La nota que más se repite es: " + sistema.getNota_Que_MasRepite());
        
    }
    
    
}
