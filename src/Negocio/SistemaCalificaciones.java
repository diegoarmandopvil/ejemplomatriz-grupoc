/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Util.LeerMatriz_Excel;
import java.io.IOException;

/**
 *  Clase del negocio para la manipulación de una lista de Calificaciones
 * @author Marco Adarme
 */
public class SistemaCalificaciones {

    public static String getMayorNotaQuices;
    
    private Estudiante []listaEstructuras;

    public SistemaCalificaciones() {
    }
    
    
    public SistemaCalificaciones(String nombreArchivo) throws IOException {
        LeerMatriz_Excel miExcel=new LeerMatriz_Excel(nombreArchivo,0);
        String miMatriz[][]=miExcel.getMatriz();
        
        //Normalizar: Pasar de un archivo a un modelo de objetos
        this.listaEstructuras=new Estudiante[miMatriz.length-1];
        crearEstudiantes(miMatriz);
        
        
    }
    
    
    private void crearEstudiantes(String datos[][])
    {
    
        for(int fila=1;fila<datos.length;fila++)
        {
            //Datos para un estudiante
            String nombre="";
            long codigo=0;
            float quices[]=new float[datos[fila].length-2];
            int indice_quices=0;
            for(int columna=0;columna<datos[fila].length;columna++)
            {
            
                
                if(columna==0)
                    codigo=Long.parseLong(datos[fila][columna]);
                else
                {
                    if(columna==1)
                        nombre=datos[fila][columna];
                    else
                    {
                        quices[indice_quices]=Float.parseFloat(datos[fila][columna]);
                        indice_quices++;
                    }
                }
                
            }
            //Creó al estudiante
            Estudiante nuevo=new Estudiante(codigo, nombre);
            nuevo.setQuices(quices);
            //Ingresar al listado de Estudiantes:
            this.listaEstructuras[fila-1]=nuevo;
            
        
        }
          
        
    }
    
    public Estudiante[] getListaEstructuras() {
        return listaEstructuras;
    }
    
    
     public SistemaCalificaciones(int can) {
         
         // T identi[] = new T[tamaño];
         //T es Estudiante
            this.listaEstructuras=new Estudiante[can];
    }
    
    
     //Esto va a cambiar
     public void insertarEstudiante_EnPos0(long codigo,String nombre,  String notas)
     {
         Estudiante nuevo=new Estudiante(codigo, nombre, notas);
         
         this.listaEstructuras[0]=nuevo;
         
     }

    @Override
    public String toString() {
        
        String msg="Mis estudiantes son:\n";
        
        for(Estudiante unEstudiante:this.listaEstructuras)
            msg+=unEstudiante.toString()+"\n";
        
        return msg;
        
    }
     
     
    /**
     * Obtiene los estudiantes cuyo promedio de quices es menor a 3
     * @return un vector de objetos de la clase Estudiante
     */
    public Estudiante[] getReprobaronQuices()
    {
        Estudiante [] estudiantesPerdidos = new Estudiante[this.getContadorPerdidos()];
        int cont=0;
        for(int i=0; i<this.listaEstructuras.length; i++){
            Estudiante estudiante = this.listaEstructuras[i];
            float promEstudiante = estudiante.getPromedio();
            if(promEstudiante<3.0F){
                estudiantesPerdidos[cont++] = estudiante;
            }
        }
        return estudiantesPerdidos;
    }

    /**
     * Realiza un conteo para obtener la cantidad de estudiantes que perdieron
     * @return un entero con la cantidad de alumnos reprobados
     */
    public int getContadorPerdidos(){
        int cont=0;
        for(int i=0; i<this.listaEstructuras.length; i++){
            float promedioEstudiante = this.listaEstructuras[i].getPromedio();
            if(promedioEstudiante<3.0F){
                cont++;
            }
        }
        return cont;
    }
    
    
    
    /**
     * Obtiene los estudiantes cuyo promedio de quices es mayor o igual a 4
     * @return un vector de objetos de la clase Estudiante
     */
    public Estudiante[] getMayorNotaQuices()
    {
        Estudiante [] estudiantesAprobados = new Estudiante[this.getContadorAprobados()];
        int cont=0;
        for(int i=0; i<this.listaEstructuras.length; i++){
            Estudiante estudiante = this.listaEstructuras[i];
            float promedioEstudiante = estudiante.getPromedio();
            if(promedioEstudiante>=4.0F){
                estudiantesAprobados[cont++] = estudiante;
            }
        }
        return estudiantesAprobados;
    }

    /**
     * Realiza un conteo para obtener la cantidad de estudiantes que aprobaron
     * @return un entero con la cantidad de alumnos aprobados
     */
    public int getContadorAprobados(){
        int cont=0;
        for(int i=0; i<this.listaEstructuras.length; i++){
            float promEstudiante = this.listaEstructuras[i].getPromedio();
            if(promEstudiante>=4.0F){
                cont++;
            }
        }
        return cont;      
    }
    
    /**
     * Obtiene el nombre del quiz que más perdieron los estudiantes (q1, q2....qn)
     * @return un String con el nombre de la columna en Excel
     */
    public String getNombreQuiz_Perdieron()
    {
    
        return "";
    }
    
    
    /**
     * Obtiene la nota que más se repite  (Ojo suponga notas de un entero y un decimal
     * @return un float con la nota que más se repite
     */    
    public float getNota_Que_MasRepite()
    {
        int vecesQueSeRepite = 0;
        int mayorRepetida = 0;
        float mayor =0;
        float [][] notas = this.getNotasOrdenadas();
        // [26][4] 
        for(int i=0; i<notas.length; i++){
            for(int j=0; j<notas[i].length; j++){
                float nota = notas[i][j];
                vecesQueSeRepite = this.vecesQueRepite(nota, notas);
                //10 2
                if(vecesQueSeRepite > mayorRepetida){
                    mayor = nota;
                    mayorRepetida = vecesQueSeRepite;
                }
            }
        }
        return mayor;
    }

    
    /**
     * Realiza el conteo de cuantas veces se repite una nota
     * @return un entero con la cantidad de veces que se repite una nota
     */  
    public int vecesQueRepite(float nota, float [][] notasOrdenadas){
        float [][] notas = this.getNotasOrdenadas();
        int cont=-1;
        for(int i=0; i<notas.length; i++){
            for(int j=0; j<notas[i].length; j++){
                if(nota == notas[i][j])
                    cont++;
            }
        }
        return cont;
    }

    
    /**
     * Ordena las notas para que sea mas facil realizar el conteo
     * @return una matriz con todas las notas ordenadas
     */  
    public float[][] getNotasOrdenadas(){
        float [][] notasEnOrden = new float[this.listaEstructuras.length][];
        for(int i=0; i<this.listaEstructuras.length; i++){
            Estudiante e = this.listaEstructuras[i];
            notasEnOrden[i] = new float[e.getQuices().length];
            for(int j=0; j<notasEnOrden[i].length; j++){
                notasEnOrden[i][j] = e.getQuices()[j];
            }
        }
        return notasEnOrden;
    }
    
     
    
}
